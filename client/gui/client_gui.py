# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'client/gui/client.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(630, 512)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.sem1 = QtWidgets.QGraphicsView(self.centralwidget)
        self.sem1.setObjectName("sem1")
        self.verticalLayout_2.addWidget(self.sem1)
        self.video1 = QtWidgets.QGraphicsView(self.centralwidget)
        self.video1.setObjectName("video1")
        self.verticalLayout_2.addWidget(self.video1)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.sem2 = QtWidgets.QGraphicsView(self.centralwidget)
        self.sem2.setObjectName("sem2")
        self.verticalLayout_3.addWidget(self.sem2)
        self.video2 = QtWidgets.QGraphicsView(self.centralwidget)
        self.video2.setObjectName("video2")
        self.verticalLayout_3.addWidget(self.video2)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))


