# Rodrigo Custodio
# Jose Antonio Ramirez
# Sebastian Galindo

from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, Qt
from PyQt5.QtGui import QImage
import cv2


class CarVideo(QThread):
    """
    Object to represent a opencv video
    """
    change_image = pyqtSignal("QImage", int)

    def __init__(self, filename, classifier, signal):
        super(QThread, self).__init__()
        self.filename = filename
        self.carcsc = cv2.CascadeClassifier(classifier)
        signal.connect(self.action)
        self.pause = True

    @pyqtSlot("QString")
    def action(self, action):
        if (action == "r"):
            self.pause = True
        elif (action == "y"):
            self.pause = False
        elif (action == "g"):
            self.pause = False

    def run(self):
        cap = cv2.VideoCapture(self.filename)
        frame_counter = 0
        last_frame = cap.get(cv2.CAP_PROP_FRAME_COUNT)
        pause_img = None
        while True:
            self.msleep(30)
            ret, frame = cap.read()
            frame_counter += 1
            if frame_counter == last_frame:
                frame_counter = 0
                cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            if ret:
                rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                cars = self.carcsc.detectMultiScale(rgb, 1.1, 1)
                if self.pause and pause_img is not None:
                    self.change_image.emit(pause_img, len(cars))
                    continue
                for (x, y, w, h) in cars:
                    frame = cv2.rectangle(rgb, (x, y), (x + w, y + h),
                                          (244, 241, 61), 2)
                height, width, ch = rgb.shape
                img = QImage(rgb.data, width, height, ch * width,
                             QImage.Format_RGB888)
                img = img.scaled(300, 300, Qt.KeepAspectRatio)
                pause_img = img
                self.change_image.emit(img, len(cars))
