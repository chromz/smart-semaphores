# Rodrigo Custodio
# Jose Antonio Ramirez
# Sebastian Galindo

import socketio


class Namespace(socketio.ClientNamespace):
    """
    Socket io namespace
    """
    def __init__(self, namespace, signal):
        super(socketio.ClientNamespace, self).__init__(namespace)
        self.signal = signal

    def on_change(self, data):
        print("Emitting", data)
        self.signal.emit(data[0], data[1])
