# Rodrigo Custodio
# Jose Antonio Ramirez
# Sebastian Galindo#

from PyQt5.QtWidgets import QGraphicsEllipseItem, QGraphicsRectItem
from PyQt5.QtGui import QColor, QBrush
from PyQt5.QtCore import QRectF


class Semaphore:
    """
    Class that self draws a semaphore
    """
    def __init__(self, x, y, width, height, scene):
        self.color = "n"
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.scene = scene
        self.red_off = (0, 0, 0)
        self.red_on = (255, 93, 93)
        self.yellow_off = (0, 0, 0)
        self.yellow_on = (255, 252, 80)
        self.green_off = (0, 0, 0)
        self.green_on = (75, 243, 91)

    def draw(self):

        brush = QBrush(QColor(0, 0, 0))
        margin_bottom = 19
        base_height = (self.height + margin_bottom) * 2 + 2 * self.height
        base = QRectF(self.x - self.width / 2, self.y - self.height / 2,
                      self.width * 2, base_height)
        red = QRectF(self.x, self.y, self.width, self.height)
        yellow = QRectF(self.x, self.y + (margin_bottom + self.height),
                        self.width, self.height)
        green = QRectF(self.x, self.y + 2 * (margin_bottom + self.height),
                       self.width, self.height)

        self.sem_base = QGraphicsRectItem(base)
        self.sem_base.setBrush(brush)

        brush.setColor(QColor(*self.red_off))
        self.red_circle = QGraphicsEllipseItem(red)
        self.red_circle.setBrush(brush)

        brush.setColor(QColor(*self.yellow_off))
        self.yellow_circle = QGraphicsEllipseItem(yellow)
        self.yellow_circle.setBrush(brush)

        brush.setColor(QColor(*self.green_off))
        self.green_circle = QGraphicsEllipseItem(green)
        self.green_circle.setBrush(brush)

        # Add items
        self.scene.addItem(self.sem_base)
        self.scene.addItem(self.red_circle)
        self.scene.addItem(self.yellow_circle)
        self.scene.addItem(self.green_circle)

    def change_color(self, col="n"):
        if (col == "n"):
            brush = QBrush(QColor(*self.red_off))
            self.red_circle.setBrush(brush)
            brush = QBrush(QColor(*self.yellow_off))
            self.yellow_circle.setBrush(brush)
            brush = QBrush(QColor(*self.green_off))
            self.green_circle.setBrush(brush)
        elif (col == "r"):
            brush = QBrush(QColor(*self.red_on))
            self.red_circle.setBrush(brush)
            brush = QBrush(QColor(*self.yellow_off))
            self.yellow_circle.setBrush(brush)
            brush = QBrush(QColor(*self.green_off))
            self.green_circle.setBrush(brush)
        elif (col == "y"):
            brush = QBrush(QColor(*self.red_off))
            self.red_circle.setBrush(brush)
            brush = QBrush(QColor(*self.yellow_on))
            self.yellow_circle.setBrush(brush)
            brush = QBrush(QColor(*self.green_off))
            self.green_circle.setBrush(brush)
        elif (col == "g"):
            brush = QBrush(QColor(*self.red_off))
            self.red_circle.setBrush(brush)
            brush = QBrush(QColor(*self.yellow_off))
            self.yellow_circle.setBrush(brush)
            brush = QBrush(QColor(*self.green_on))
            self.green_circle.setBrush(brush)
