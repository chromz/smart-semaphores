# Rodrigo Custodio
# Jose Antonio Ramirez
# Sebastian Galindo

from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot
from gui.namespace import Namespace
import socketio


class SocketWorker(QThread):
    """
    Client socket train
    """
    sio = socketio.Client()
    change = pyqtSignal("QString", "QString")

    def __init__(self, update_signal, host="127.0.0.1", port=3000):
        super(QThread, self).__init__()
        update_signal.connect(self.update_socket)
        self.host = host
        self.port = port
        self.httpurl = "http://{}:{}".format(host, port)

    @pyqtSlot(list)
    def update_socket(self, car_flow):
        self.sio.emit("update", {
            "cars": car_flow
        }, namespace="/sem")

    def run(self):
        self.sio.register_namespace(Namespace("/sem", self.change))
        self.sio.connect(self.httpurl)
        self.sio.wait()
