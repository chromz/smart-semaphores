# Rodrigo Custodio
# Jose Antonio Ramirez
# Sebastian Galindo

from PyQt5.QtWidgets import QMainWindow, QGraphicsScene, QGraphicsPixmapItem
from PyQt5.QtOpenGL import QGLWidget
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtGui import QPixmap
from gui.client_gui import Ui_MainWindow
from gui.semaphore import Semaphore
from gui.socket import SocketWorker
from gui.opencv_video import CarVideo


class MainWindow(QMainWindow, Ui_MainWindow):
    """
    Main Gui of server
    """
    video1_signal = pyqtSignal("QString")
    video2_signal = pyqtSignal("QString")
    flow_signal = pyqtSignal(list)
    treshold = 100
    count = 0

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)
        self.scene1 = QGraphicsScene()
        self.scene2 = QGraphicsScene()
        self.car_flow = [0, 0]

        # Opencv video objects
        self.pvideo1 = QGraphicsPixmapItem()
        self.pvideo2 = QGraphicsPixmapItem()
        self.video_scene1 = QGraphicsScene()
        self.video_scene2 = QGraphicsScene()
        self.video1.setScene(self.video_scene1)
        self.video2.setScene(self.video_scene2)
        self.video_scene1.addItem(self.pvideo1)
        self.video_scene2.addItem(self.pvideo2)
        self.cvvideo1 = CarVideo('videos/video1.avi', 'data/cars.xml',
                                 self.video1_signal)
        self.cvvideo2 = CarVideo('videos/video2.avi', 'data/cars.xml',
                                 self.video2_signal)
        self.cvvideo1.change_image.connect(self.update_video1)
        self.cvvideo2.change_image.connect(self.update_video2)

        self.sem1.setScene(self.scene1)
        self.sem2.setScene(self.scene2)
        # self.scene1.setSceneRect(0, 0, 800, 600)

        # Enable OpenGL acceleration
        self.sem1.setViewport(QGLWidget())
        self.sem2.setViewport(QGLWidget())
        self.video1.setViewport(QGLWidget())
        self.video2.setViewport(QGLWidget())

        # Draw the semaphore on screen
        width, height = 20, 20
        x = self.scene1.width() / 2 - width
        y = self.scene1.height() / 2
        self.left_semaphore = Semaphore(x, y, width, height, self.scene1)
        self.right_semaphore = Semaphore(x, y, width, height, self.scene2)
        self.draw_semaphores()

        self.socket_thread = SocketWorker(self.flow_signal)
        self.socket_thread.change.connect(self.change_color)
        self.socket_thread.start()
        self.cvvideo1.start()
        self.cvvideo2.start()

        self.show()

    @pyqtSlot("QString", "QString")
    def change_color(self, col1, col2):
        self.video1_signal.emit(col1)
        self.video2_signal.emit(col2)
        self.left_semaphore.change_color(col1)
        self.right_semaphore.change_color(col2)

    @pyqtSlot("QImage", int)
    def update_video1(self, img, cars):
        self.car_flow[0] = cars
        self.count += 1
        if self.count == self.treshold:
            self.flow_signal.emit(self.car_flow)
            self.count = 0
        self.pvideo1.setPixmap(QPixmap.fromImage(img))

    @pyqtSlot("QImage", int)
    def update_video2(self, img, cars):
        self.car_flow[1] = cars
        self.count += 1
        if self.count == self.treshold:
            self.flow_signal.emit(self.car_flow)
            self.count = 0
        self.pvideo2.setPixmap(QPixmap.fromImage(img))

    def draw_semaphores(self):
        self.left_semaphore.draw()
        self.right_semaphore.draw()
