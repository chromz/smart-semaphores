# Rodrigo Custodio
# Jose Antonio Ramirez
# Sebastian Galindo

import os
from PyQt5.QtWidgets import QApplication

if __name__ == "__main__":
    if __debug__:
        os.system("pyuic5 client/gui/client.ui > client/gui/client_gui.py")
    # Laziness
    from gui.main_window import MainWindow
    app = QApplication([])
    window = MainWindow()
    app.exec_()
