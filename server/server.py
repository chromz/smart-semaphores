# Rodrigo Custodio
# Jose Antonio Ramirez
# Sebastian Galindo

from aiohttp import web
import socketio
import asyncio

sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)
enable_algorithm = False

traffic_constant = 1.0
base_time = 5
current_time = 5

current_sems = ["r", "g"]
inter_sid = None


async def interval():
    while True:
        await swap_colors()
        if current_sems[0] == "g":
            current_time = traffic_constant * base_time
        if current_time == 0:
            current_time = base_time
        print("Current time", current_time)
        await asyncio.sleep(current_time)


async def swap_colors():
    if current_sems[0] == "r":
        current_sems[0] = "g"
        current_sems[1] = "r"
    elif current_sems[0] == "g":
        current_sems[0] = "r"
        current_sems[1] = "g"
    await sio.emit("change", data=current_sems, room=inter_sid,
                   namespace="/sem")


@sio.on("connect", namespace="/sem")
async def connect(sid, environ):
    print("Connect", sid)
    inter_sid = sid
    await sio.emit("change", data=current_sems, room=inter_sid,
                   namespace="/sem")
    if not enable_algorithm:
        loop = asyncio.get_event_loop()
        loop.create_task(interval())


@sio.on("update", namespace="/sem")
async def update(sid, cars):
    global traffic_constant
    sem1 = cars["cars"][0]
    sem2 = cars["cars"][1]
    if (sem2 == 0):
        traffic_constant = 0.5
        return
    traffic_constant = sem1 / sem2


if __name__ == "__main__":
    web.run_app(app, port=3000)
